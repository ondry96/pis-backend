package cz.vutbr.fit.PISBE.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthenticationConfiguration {
    public AuthenticationConfiguration() {
    }

    @Bean
    public Auth getFilter() {
        return new Auth();
    }
}
