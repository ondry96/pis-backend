package cz.vutbr.fit.PISBE.configuration;

import java.util.List;

public class Principal {
    private String email;
    private List<String> roles;

    public Principal(String email, List<String> roles) {
        this.email = email;
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public List<String> getRoles() {
        return roles;
    }
}
