package cz.vutbr.fit.PISBE.configuration;

import cz.vutbr.fit.PISBE.PisBeApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PisBeApplication.class);
    }

}

