package cz.vutbr.fit.PISBE.jsonObjects;

import cz.vutbr.fit.PISBE.features.role.Role;

import java.util.Set;

public class ResponseBodyMessageJackson {
    private String message;

    public ResponseBodyMessageJackson() {
    }

    public ResponseBodyMessageJackson(String message) {
        this.message = message;
    }

    public ResponseBodyMessageJackson(Set<Role> roles) {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
