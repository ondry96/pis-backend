package cz.vutbr.fit.PISBE.authentication;


import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.features.user.UserServiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
public class AuthenticationController {

    @Autowired
    UserServiceDao userServiceDao;

    @PostMapping(value="/login")
    public Object loginUser(@RequestBody String json, HttpServletResponse response) {
        return userServiceDao.loginUserFromJson(json).setup(response);
    }

    @PostMapping(value="/register")
    public Object registerUser(@RequestBody String json, HttpServletResponse response) {
        return userServiceDao.createNewUserFromJson(json).setup(response);
    }
}
