package cz.vutbr.fit.PISBE.features.placeOfReservation;

import cz.vutbr.fit.PISBE.common.ResponseHolder;

public interface PlaceOfReservationServiceDao {

    ResponseHolder getTablesSchemes(String type);

    ResponseHolder getFreeTables(String json);

}
