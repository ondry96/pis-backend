package cz.vutbr.fit.PISBE.features.category.jsonObjects;

import java.util.ArrayList;

public class GetMenuDetailsJackson {
    private ArrayList<ShowMenuJackson> categoriesInMenu;
    private ArrayList<ShowMenuJackson> categories;

    public GetMenuDetailsJackson(ArrayList<ShowMenuJackson> categoriesInMenu, ArrayList<ShowMenuJackson> categories) {
        this.categoriesInMenu = categoriesInMenu;
        this.categories = categories;
    }

    public GetMenuDetailsJackson() {
    }

    public ArrayList<ShowMenuJackson> getCategoriesInMenu() {
        return categoriesInMenu;
    }

    public void setCategoriesInMenu(ArrayList<ShowMenuJackson> categoriesInMenu) {
        this.categoriesInMenu = categoriesInMenu;
    }

    public ArrayList<ShowMenuJackson> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<ShowMenuJackson> categories) {
        this.categories = categories;
    }
}
