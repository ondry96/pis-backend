package cz.vutbr.fit.PISBE.features.reservation.jsonObjects;

import cz.vutbr.fit.PISBE.features.user.jsonObjects.HostInfoJackson;

public class ReservationForLoggedUserJackson {
    private ReservationInfoJackson reservationInfo;
    private HostInfoJackson hostInfo;
    private int placeId;

    public ReservationForLoggedUserJackson() {
    }

    public ReservationForLoggedUserJackson(ReservationInfoJackson reservationInfo, HostInfoJackson hostInfo, int placeId) {
        this.reservationInfo = reservationInfo;
        this.hostInfo = hostInfo;
        this.placeId = placeId;
    }

    public ReservationInfoJackson getReservationInfo() {
        return reservationInfo;
    }

    public void setReservationInfo(ReservationInfoJackson reservationInfo) {
        this.reservationInfo = reservationInfo;
    }

    public HostInfoJackson getHostInfo() {
        return hostInfo;
    }

    public void setHostInfo(HostInfoJackson hostInfo) {
        this.hostInfo = hostInfo;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }
}
