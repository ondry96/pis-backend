package cz.vutbr.fit.PISBE.features.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.ResponseHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
public class UserController {

    @Autowired
    UserServiceDao userServiceDao;

    @PostMapping(value = "/employees/new")
    public Object addNewEmployee(@RequestBody String json, HttpServletResponse response) {
        return userServiceDao.addNewEmployeeFromJson(json).setup(response);
    }

    @GetMapping(value = "/employees/edit")
    public Object getAllEmployees(HttpServletResponse response){
        return userServiceDao.getAllEmployees().setup(response);
    }

    @DeleteMapping(value = "/employee/{email}/delete")
    public Object deleteEmployee(HttpServletResponse response, @PathVariable String email) {
        return userServiceDao.deleteEmployee(email).setup(response);
    }

    @PutMapping(value = "/employee/{email}/editAttribute")
    public Object editEmployee(@RequestBody String json, HttpServletResponse response, @PathVariable String email) throws JsonProcessingException {
        return userServiceDao.editEmployee(json, email).setup(response);
    }

    @PutMapping(value = "/employee/{email}/changeRoleState")
    public Object editEmployeesRole(@RequestBody String json, HttpServletResponse response, @PathVariable String email) throws JsonProcessingException {
        return userServiceDao.editEmployeesRole(json, email).setup(response);
    }

    @GetMapping(value = "/account")
    public Object getUserInfo(HttpServletResponse response) {
        return userServiceDao.getUserInfo().setup(response);
    }

    @PutMapping(value = "/account/{email}")
    public Object editUserAccount(@RequestBody String requestBody, HttpServletResponse response,  @PathVariable String email) {
        return userServiceDao.editUserAccount(requestBody, email).setup(response);
    }
}
