package cz.vutbr.fit.PISBE.features.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
    Reservation findByReservationId(int reservationId);

    @Query(nativeQuery = true, value = "select POR2.PLACEID from PLACE_OF_RESERVATION POR2 MINUS select POR.PLACEID from PLACE_OF_RESERVATION POR JOIN RESERVATION\n" +
            "    on RESERVATION.PLACE_OF_RESERVATION_REFERENCE =" +
            " POR.PLACEID  where (RESERVATION.START_OF_RESERVATION_DATE BETWEEN ?1" +
            "    and ?2  or RESERVATION.END_OF_RESERVATION_DATE\n" +
            "    BETWEEN ?1 and ?2 or ?1 BETWEEN RESERVATION.START_OF_RESERVATION_DATE and RESERVATION.END_OF_RESERVATION_DATE\n" +
            "or ?2 BETWEEN RESERVATION.START_OF_RESERVATION_DATE and RESERVATION.END_OF_RESERVATION_DATE) and RESERVATION.STATE like 'APPROVED' group by POR.PLACEID")
    List<Integer> getAllFreePlacesByPlaceInTime(Date startDate, Date endDate);


    @Query(nativeQuery = true, value = "select POR.PLACEID from PLACE_OF_RESERVATION POR JOIN RESERVATION\n" +
            "    on RESERVATION.PLACE_OF_RESERVATION_REFERENCE =" +
            " POR.PLACEID  where (RESERVATION.START_OF_RESERVATION_DATE BETWEEN ?1" +
            "    and ?2  or RESERVATION.END_OF_RESERVATION_DATE\n" +
            "    BETWEEN ?1 and ?2 or ?1 BETWEEN RESERVATION.START_OF_RESERVATION_DATE and RESERVATION.END_OF_RESERVATION_DATE\n" +
            "or ?2 BETWEEN RESERVATION.START_OF_RESERVATION_DATE and RESERVATION.END_OF_RESERVATION_DATE) and RESERVATION.STATE like 'APPROVED' group by POR.PLACEID")
    List<Integer> getAllOccupiedPlacesByPlaceInTime(Date startDate, Date endDate);

}
