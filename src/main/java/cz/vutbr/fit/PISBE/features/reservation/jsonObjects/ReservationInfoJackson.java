package cz.vutbr.fit.PISBE.features.reservation.jsonObjects;

public class ReservationInfoJackson {
    private String date;
    private String time;
    private int expectedLength;
    private int hostsCount;

    public ReservationInfoJackson() {
    }

    public ReservationInfoJackson(String date, String time, int expectedLength, int hostsCount) {
        this.date = date;
        this.time = time;
        this.expectedLength = expectedLength;
        this.hostsCount = hostsCount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getExpectedLength() {
        return expectedLength;
    }

    public void setExpectedLength(int expectedLength) {
        this.expectedLength = expectedLength;
    }

    public int getHostsCount() {
        return hostsCount;
    }

    public void setHostsCount(int hostsCount) {
        this.hostsCount = hostsCount;
    }
}



