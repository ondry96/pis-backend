package cz.vutbr.fit.PISBE.features.menuItem.jsonObjects;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class MenuItemSalesJackson {
    private int menuItemId;
    private String name;
    private double price;
    private double productionPrice;
    private int inMenu;
    private String grammage;
    private String description;
    private int soldCount;
    private ArrayList<String> soldTimes;
    private String lastTimeSold;

    public MenuItemSalesJackson() {
    }

    public MenuItemSalesJackson(int menuItemId, String name, double price, double productionPrice, int inMenu, String grammage, String description, int soldCount) {
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
        this.productionPrice = productionPrice;
        this.inMenu = inMenu;
        this.grammage = grammage;
        this.description = description;
        this.soldCount = soldCount;
    }

    public MenuItemSalesJackson(int menuItemId, String name, double price, double productionPrice, int inMenu, String grammage, String description, ArrayList<String> soldTimes) {
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
        this.productionPrice = productionPrice;
        this.inMenu = inMenu;
        this.grammage = grammage;
        this.description = description;
        this.soldTimes = soldTimes;
    }

    public MenuItemSalesJackson(int menuItemId, String name, double price, double productionPrice, int inMenu, String grammage, String description, String lastTimeSold) {
        this.menuItemId = menuItemId;
        this.name = name;
        this.price = price;
        this.productionPrice = productionPrice;
        this.inMenu = inMenu;
        this.grammage = grammage;
        this.description = description;
        this.lastTimeSold = lastTimeSold;
    }

    public int getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getProductionPrice() {
        return productionPrice;
    }

    public void setProductionPrice(double productionPrice) {
        this.productionPrice = productionPrice;
    }

    public int getInMenu() {
        return inMenu;
    }

    public void setInMenu(int inMenu) {
        this.inMenu = inMenu;
    }

    public String getGrammage() {
        return grammage;
    }

    public void setGrammage(String grammage) {
        this.grammage = grammage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSoldCount() {
        return soldCount;
    }

    public void setSoldCount(int soldCount) {
        this.soldCount = soldCount;
    }

    public ArrayList<String> getSoldTimes() {
        return soldTimes;
    }

    public void setSoldTimes(ArrayList<String> soldTimes) {
        this.soldTimes = soldTimes;
    }

    public void updateSoldTimes(String newValue){
        this.soldTimes.add(newValue);
    }

    public void sortSoldTimes(){
        this.soldTimes.sort(new Comparator<String>() {
            @Override
            public int compare(String object1, String object2) {
                return object1.compareTo(object2);
            }
        });
    }

    public String getLastTimeSold() {
        return lastTimeSold;
    }

    public void setLastTimeSold(String lastTimeSold) {
        this.lastTimeSold = lastTimeSold;
    }
}
