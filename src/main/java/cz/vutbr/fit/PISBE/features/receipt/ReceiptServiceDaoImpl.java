package cz.vutbr.fit.PISBE.features.receipt;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.ExceptionHandling;
import cz.vutbr.fit.PISBE.common.JsonToObjectParser;
import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.features.category.Category;
import cz.vutbr.fit.PISBE.features.category.CategoryRepository;
import cz.vutbr.fit.PISBE.features.orderItem.OrderItem;
import cz.vutbr.fit.PISBE.features.orderItem.OrderItemRepository;
import cz.vutbr.fit.PISBE.features.orderItem.State;
import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservation;
import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservationRepository;
import cz.vutbr.fit.PISBE.features.receipt.jsonObjects.PaymentJackson;
import cz.vutbr.fit.PISBE.features.receipt.jsonObjects.ReceiptJackson;
import cz.vutbr.fit.PISBE.features.receipt.jsonObjects.ReceiptSortByCategoryJackson;
import cz.vutbr.fit.PISBE.features.user.UserServiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReceiptServiceDaoImpl implements ReceiptServiceDao {

    @Autowired
    UserServiceDao userServiceDao;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    OrderItemRepository orderItemRepository;

    @Autowired
    PlaceOfReservationRepository placeOfReservationRepository;

    @Autowired
    ReceiptRepository receiptRepository;

    @Autowired
    ExceptionHandling exceptionHandling;

    public ResponseHolder getBillingForm(int placeId) {
        if (!userServiceDao.canAccess("Waiter")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        List<Category> categories = categoryRepository.findAll();
        PlaceOfReservation placeOfReservation = placeOfReservationRepository.getOne(placeId);
        ArrayList<ReceiptSortByCategoryJackson> bill = new ArrayList<>();

        for (Category category : categories) {

            List<OrderItem> orderItems = orderItemRepository.findAllByPlaceOfReservationReference(placeOfReservation);

            ArrayList<ReceiptJackson> itemsOfBill = new ArrayList<>();

            for (OrderItem orderItem : orderItems) {
                if(orderItem.getMenuItemReference().getCategoryReference().getName().equals(category.getName())) {
                    if (!(orderItem.getState() == State.PAID)){
                        ReceiptJackson itemsOfBillJackson = new ReceiptJackson(orderItem.getOrderItemId(),
                                orderItem.getMenuItemReference().getMenuItemId(), orderItem.getMenuItemReference().getName(),
                                orderItem.getMenuItemReference().getPrice(), orderItem.getSpecialRequirement(),
                                orderItem.getMenuItemReference().getGrammage(), orderItem.getState());
                        itemsOfBill.add(itemsOfBillJackson);
                    }
                }

            }
            ReceiptSortByCategoryJackson itemsOfBillJackon = new ReceiptSortByCategoryJackson(category.getName(), itemsOfBill);
            if(orderItems.size() > 0) {
                bill.add(itemsOfBillJackon);
            }
        }

        ResponseHolder<ArrayList<ReceiptSortByCategoryJackson>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(bill);
        return responseHolder;
    }


    public ResponseHolder makePayment(String requestBody, int placeId) throws ParseException {
        if (!userServiceDao.canAccess("Waiter")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        PaymentJackson orderItemIds;
        try {
            orderItemIds = JsonToObjectParser.parseJsonToObject(requestBody, PaymentJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Chyba při zpracovávání platby.");
        }


        String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;

        date = dateFormat.parse(dateString);

        Receipt newReceipt = new Receipt(date);

        try {
            receiptRepository.save(newReceipt);
        } catch (Exception e) {
            return exceptionHandling.handleDatabaseException(e);
        }

        for (int orderItemId : orderItemIds.getOrderItemIds()) {
            OrderItem tempOrderItem = orderItemRepository.getOne(orderItemId);
            tempOrderItem.setState(State.PAID);
            tempOrderItem.setReceiptReference(newReceipt);
            try {
                orderItemRepository.save(tempOrderItem);
            } catch (Exception e) {
                return exceptionHandling.handleDatabaseException(e);
            }

        }
        return ResponseHolder.returnEmptyBody(HttpServletResponse.SC_OK);
    }
}
