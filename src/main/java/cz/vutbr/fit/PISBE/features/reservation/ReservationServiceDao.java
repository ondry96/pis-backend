package cz.vutbr.fit.PISBE.features.reservation;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.ResponseHolder;

public interface ReservationServiceDao {

    ResponseHolder getAllReservations();

    ResponseHolder addNewReservation(String json);

    ResponseHolder addNewReservationForLoggedUserAndWaiter(String json);

    ResponseHolder editReservation(int reservationId, String requestBody);

    ResponseHolder cancelReservationByCode(String json) throws JsonProcessingException;
}
