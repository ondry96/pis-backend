package cz.vutbr.fit.PISBE.features.menuItem.jsonObjects;

public class MenuItemNewJackson {
    private String name;
    private double price;
    private double productionPrice;
    private String grammage;
    private String description;
    private int categoryId;

    public MenuItemNewJackson() {
    }

    public MenuItemNewJackson(String name, double price, double productionPrice, String grammage, String description, int categoryId) {
        this.name = name;
        this.price = price;
        this.productionPrice = productionPrice;
        this.grammage = grammage;
        this.description = description;
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getProductionPrice() {
        return productionPrice;
    }

    public void setProductionPrice(double productionPrice) {
        this.productionPrice = productionPrice;
    }

    public String getGrammage() {
        return grammage;
    }

    public void setGrammage(String grammage) {
        this.grammage = grammage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
