package cz.vutbr.fit.PISBE.features.reservation.jsonObjects;

import cz.vutbr.fit.PISBE.features.user.jsonObjects.HostInfoJackson;

public class ReservationForUnloggedUserJackson {
    private HostInfoJackson hostInfo;
    private ReservationInfoJackson reservationInfo;

    public ReservationForUnloggedUserJackson() {
    }

    public ReservationForUnloggedUserJackson(HostInfoJackson hostInfo, ReservationInfoJackson reservationInfo) {
        this.hostInfo = hostInfo;
        this.reservationInfo = reservationInfo;
    }

    public HostInfoJackson getHostInfo() {
        return hostInfo;
    }

    public void setHostInfo(HostInfoJackson hostInfo) {
        this.hostInfo = hostInfo;
    }

    public ReservationInfoJackson getReservationInfo() {
        return reservationInfo;
    }

    public void setReservationInfo(ReservationInfoJackson reservationInfo) {
        this.reservationInfo = reservationInfo;
    }
}


