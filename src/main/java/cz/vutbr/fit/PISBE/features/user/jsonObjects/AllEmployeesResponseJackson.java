package cz.vutbr.fit.PISBE.features.user.jsonObjects;

import java.util.ArrayList;

public class AllEmployeesResponseJackson{
    private EmployeeInfo employeeInfo;
    private ArrayList<String> roles;

    public AllEmployeesResponseJackson() {
    }

    public AllEmployeesResponseJackson(EmployeeInfo employeeInfo, ArrayList<String> roles) {
        this.employeeInfo = employeeInfo;
        this.roles = roles;
    }

    public EmployeeInfo getEmployeeInfo() {
        return employeeInfo;
    }

    public void setEmployeeInfo(EmployeeInfo employeeInfo) {
        this.employeeInfo = employeeInfo;
    }

    public ArrayList<String> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }
}
