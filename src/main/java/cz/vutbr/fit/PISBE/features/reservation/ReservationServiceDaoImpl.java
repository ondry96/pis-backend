package cz.vutbr.fit.PISBE.features.reservation;


import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.ExceptionHandling;
import cz.vutbr.fit.PISBE.common.JsonToObjectParser;
import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.configuration.Auth;
import cz.vutbr.fit.PISBE.configuration.Principal;
import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservation;
import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservationRepository;
import cz.vutbr.fit.PISBE.features.placeOfReservation.Type;
import cz.vutbr.fit.PISBE.features.reservation.jsonObjects.ReservationForLoggedUserJackson;
import cz.vutbr.fit.PISBE.features.reservation.jsonObjects.ReservationForUnloggedUserJackson;
import cz.vutbr.fit.PISBE.features.reservation.jsonObjects.ReservationJackson;
import cz.vutbr.fit.PISBE.features.user.User;
import cz.vutbr.fit.PISBE.features.user.UserRepository;
import cz.vutbr.fit.PISBE.features.user.UserServiceDao;
import cz.vutbr.fit.PISBE.features.user.jsonObjects.HostInfoJackson;
import cz.vutbr.fit.PISBE.mail.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class ReservationServiceDaoImpl implements ReservationServiceDao {
    @Autowired
    ReservationRepository reservationRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PlaceOfReservationRepository placeOfReservationRepository;
    @Autowired
    UserServiceDao userServiceDao;
    @Autowired
    NotificationService notificationService;
    @Autowired
    ExceptionHandling exceptionHandling;

    public static final long HOUR = 3600 * 1000;

    public ReservationJackson createReservationJson(Reservation reservation) {
        int expectedTime = (int) (reservation.getEndOfReservationDate().getTime() - reservation.getReservationDate().getTime()) / (60 * 60 * 1000);

        HostInfoJackson hostInfoJackson =
                new HostInfoJackson(reservation.getUserReference().getEmail(),
                        reservation.getUserReference().getFirstName(),
                        reservation.getUserReference().getSurname());
        return new ReservationJackson(reservation.getReservationId(), String.valueOf(reservation.getPlaceOfReservationReference().getType()),
                reservation.getPlaceOfReservationReference().getPlaceID(), expectedTime, reservation.getReservationDate(),
                String.valueOf(reservation.getState()), hostInfoJackson);
    }


    public ResponseHolder getAllReservations() {
        Principal me = Auth.me();
        String actualUserEmail = me.getEmail();

        List<Reservation> reservations = reservationRepository.findAll();
        ArrayList<ReservationJackson> responseJson = new ArrayList<>();

        if (userServiceDao.canAccess("Waiter")) {

            for (Reservation reservation : reservations) {
                responseJson.add(createReservationJson(reservation));
            }

        } else if (userServiceDao.canAccess("Logged user")) {

            for (Reservation reservation : reservations) {
                if (actualUserEmail.equals(reservation.getUserReference().getEmail())) {
                    responseJson.add(createReservationJson(reservation));
                }
            }

        } else {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        responseJson.sort(Comparator.comparing(ReservationJackson::getDate));

        ResponseHolder<ArrayList<ReservationJackson>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(responseJson);
        return responseHolder;
    }

    public String controlCodeUniq(String code) {
        List<Reservation> reservationsForControlCode = reservationRepository.findAll();

        for (Reservation reservation : reservationsForControlCode) {
            if (reservation.getCodeForDelete().equals(code)) {
                code = new Random().ints(10, 48, 122).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.joining());
                return controlCodeUniq(code);
            }
        }
        return code;
    }

    public ResponseHolder addNewReservation(String json) {
        ReservationForUnloggedUserJackson newReservation;
        try {
            newReservation = JsonToObjectParser.parseJsonToObject(json, ReservationForUnloggedUserJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje nebyly správně vyplněny.");
        }

        User searchedUser;

        try {
            searchedUser = userRepository.findByEmail(newReservation.getHostInfo().getEmail());
        } catch (Exception e) {
            searchedUser = null;
        }

        if ((searchedUser != null) && (searchedUser.isRegisteredUser())) {

            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_CONFLICT, "Tento email byl již registrován, pro uskutečnění rezervace s tímto emailem se prosím přihlašte.");
        } else {
            User user = new User(newReservation.getHostInfo().getEmail(), null, newReservation.getHostInfo().getName(), newReservation.getHostInfo().getSurname(), false);

            try {
                userRepository.save(user);
            } catch (Exception e) {
                return exceptionHandling.handleDatabaseException(e);
            }

            String dateString = newReservation.getReservationInfo().getDate() + " " + newReservation.getReservationInfo().getTime() + ":00";
            Date today = new Date();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = dateFormat.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if((date.getYear() - today.getYear()) > 2){
                return ResponseHolder.returnBadRequestAndMessage("Rezervaci lze vytvořit maximálně do dvou let od dnešního datumu.");
            }

            String codeForDelete = new Random().ints(10, 48, 122).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.joining());

            codeForDelete = controlCodeUniq(codeForDelete);

            Date endDate;
            if (newReservation.getReservationInfo().getExpectedLength() != 0) {
                endDate = new Date(date.getTime() + newReservation.getReservationInfo().getExpectedLength() * HOUR);

            } else {
                long hours = date.getHours();
                int expectedLenght;
                if (hours > 16) {
                    expectedLenght = (int) (23 - hours);
                } else {
                    expectedLenght = 6;
                }
                endDate = new Date(date.getTime() + expectedLenght * HOUR);
            }
            long controlOpenHours = endDate.getHours();
            if((controlOpenHours > 23) || (date.getDay() != endDate.getDay()) ) {
                return ResponseHolder.returnBadRequestAndMessage("Vaše rezervace přesáhla otevírací dobu. Otevírací doba je do 23:00. Prosím upravte délku, nebo čas vaší rezervace");
            }

            Reservation reservation = new Reservation(State.APPROVED, newReservation.getReservationInfo().getHostsCount(), date, endDate, codeForDelete, null, null);

            List<Integer> freePlaces = reservationRepository.getAllFreePlacesByPlaceInTime(date, endDate);

            if (freePlaces == null) {
                return ResponseHolder.returnBadRequestAndMessage("Žádný stůl v požadovaném termínu není volný, vyberte prosím jiný datum.");
            }

            ArrayList<PlaceOfReservation> suitablePlaces = new ArrayList<>();
            for (int placeId : freePlaces) {
                Optional<PlaceOfReservation> place = placeOfReservationRepository.findById(placeId);
                suitablePlaces.add(place.get());
            }

            suitablePlaces.sort(Comparator.comparing(PlaceOfReservation::getNumberOfSeats));

            boolean reservationSuccess = false;
            for (PlaceOfReservation suitablePlace : suitablePlaces) {
                if ((reservation.getExpectedNumberOfPeople() <= suitablePlace.getNumberOfSeats()) && (suitablePlace.getType() != Type.LOUNGE)) {
                    reservation.setPlaceOfReservationReference(suitablePlace);
                    reservationSuccess = true;
                    break;
                }
            }

            if (!reservationSuccess) {
                return ResponseHolder.returnBadRequestAndMessage("Žádný stůl v požadovaném termínu není volný, vyberte prosím jiný datum.");

            }

            try {
                notificationService.sendNotificationForReservation(codeForDelete, user.getEmail(), reservation);
            } catch (MailException e) {
                System.out.println("error mail");
            }

            reservation.setUserReference(user);
            reservationRepository.save(reservation);

            return ResponseHolder.returnOkAndMessage("Rezervace byla úspěšně vytvořena.");
        }
    }

    public ResponseHolder saveReservation(ReservationForLoggedUserJackson newReservation, User user, String codeForDelete) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        String dateString = newReservation.getReservationInfo().getDate() + " " + newReservation.getReservationInfo().getTime() + ":00";
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date today = new Date();
        if((date.getYear() - today.getYear()) > 2){
            return ResponseHolder.returnBadRequestAndMessage("Rezervaci lze vytvořit maximálně do dvou let od dnešního datumu.");
        }
        Date endDate;
        if (newReservation.getReservationInfo().getExpectedLength() != 0) {
            endDate = new Date(date.getTime() + newReservation.getReservationInfo().getExpectedLength() * HOUR);

        } else {
            long hours = date.getHours();
            int expectedLenght;
            if (hours > 16) {
                expectedLenght = (int) (23 - hours);
            } else {
                expectedLenght = 6;
            }
            endDate = new Date(date.getTime() + expectedLenght * HOUR);
        }

        PlaceOfReservation placeOfReservation = placeOfReservationRepository.findByPlaceID(newReservation.getPlaceId());

        Reservation reservation = new Reservation(State.APPROVED, newReservation.getReservationInfo().getHostsCount(), date, endDate, codeForDelete, user, placeOfReservation);

        if (!userServiceDao.canAccess("Waiter")) {
            try {
                notificationService.sendNotificationForReservation(codeForDelete, user.getEmail(), reservation);
            } catch (MailException e) {
                System.out.println("error mail");
            }
        }

        try {
            reservationRepository.save(reservation);
        } catch (Exception e) {
            return exceptionHandling.handleDatabaseException(e);
        }

        return null;
    }


    public ResponseHolder addNewReservationForLoggedUserAndWaiter(String json) {
        ResponseHolder saving = null;
        ReservationForLoggedUserJackson newReservation;
        try {
            newReservation = JsonToObjectParser.parseJsonToObject(json, ReservationForLoggedUserJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje nebyly správně vyplněny.");
        }

        if (userServiceDao.canAccess("Logged user")) {

            String codeForDelete = new Random().ints(10, 48, 122).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.joining());

            List<User> tests = userRepository.findAllByEmail(newReservation.getHostInfo().getEmail());
            User user = null;
            for (User test:tests) {
                if(test.isRegisteredUser()) {
                    user = test;
                }
            }

            saveReservation(newReservation, user, codeForDelete);

        } else if (userServiceDao.canAccess("Waiter")) {
            if ((newReservation.getHostInfo().getName() == null) || (newReservation.getHostInfo().getSurname() == null)) {
                //rezervace po telefonu pro uzivatele bez emailu
                User user = userRepository.findByEmail("notRegistredUser@restaurant.cz");
                PlaceOfReservation placeOfReservation = placeOfReservationRepository.findByPlaceID(newReservation.getPlaceId());
                saving = saveReservation(newReservation, user, "NOT REGISTRED USER");
            } else {
                //rezervace po telefonu na zaklade emalu
                String codeForDelete = new Random().ints(10, 48, 122).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.joining());
                if (userRepository.findByEmail(newReservation.getHostInfo().getEmail()) != null) {
                    // uzivatel je jiz registrovan - tato rezervace po telefonu mu bude prirazena k jeho emailu
                    User user = userRepository.findByEmail(newReservation.getHostInfo().getEmail());
                    try {
                        userRepository.save(user);
                    } catch (Exception e) {
                        return exceptionHandling.handleDatabaseException(e);
                    }
                    saving = saveReservation(newReservation, user, codeForDelete);
                } else {
                    // uzivatel neni registrovan bude mu vytvoren jednorazovy profil
                    User user = new User(newReservation.getHostInfo().getEmail(), "NOT REGISTRED USER", newReservation.getHostInfo().getName(), newReservation.getHostInfo().getSurname(), false);
                    try {
                        userRepository.save(user);
                    } catch (Exception e) {
                        return exceptionHandling.handleDatabaseException(e);
                    }
                    saving = saveReservation(newReservation, user, codeForDelete);
                }
            }
        } else {
            return ResponseHolder.returnBadRequestAndMessage("K provedení této akce nemáte dostatečná oprávnění.");
        }

        if (saving == null) {
            return ResponseHolder.returnOkAndMessage("Rezervace byla úspěšně vytvořena.");
        } else if (saving.equals("openHours")) {
                return ResponseHolder.returnOkAndMessage("Vaše rezervace přesáhla otevírací dobu. Vaší rezervaci jsme přijali a upravili vzhledem na otevírací dobu restaurace.");
        } else {
            return ResponseHolder.returnBadRequestAndMessage("Nastala chyba při ukládání rezervace.");
        }
    }

    public ResponseHolder editReservation(int reservationId, String requestBody) {
        if (!userServiceDao.canAccess("Waiter,Logged user")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        ReservationJackson reservationState;
        try {
            reservationState = JsonToObjectParser.parseJsonToObject(requestBody, ReservationJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje nebyly správně vyplněny.");
        }

        Principal me = Auth.me();
        String loggedUsersEmail = me.getEmail();

        if (userServiceDao.canAccess("Logged user")) {
            if (!loggedUsersEmail.equals(reservationRepository.findByReservationId(reservationId).getUserReference().getEmail())) {
                return ResponseHolder.returnBadRequestAndMessage("Uživatel nemá oprávnění měnit tuto rezervaci.");
            }

            if (reservationRepository.findByReservationId(reservationId).getState().equals(State.FINISHED)) {
                return ResponseHolder.returnBadRequestAndMessage("Tuto rezervaci již nelze zrušit přes systém. Pro zrušení rezervace nás prosím kontaktujte telefonicky.");
            }

            if (reservationRepository.findByReservationId(reservationId).getState().equals(State.APPROVED) && (reservationState.getReservationState().equals("FINISHED"))) {
                return ResponseHolder.returnBadRequestAndMessage("Pro tuto akci nemáte dostatečná oprávnění.");
            }
        }

        Reservation reservation = reservationRepository.findByReservationId(reservationId);
        if (reservation == null) {
            return ResponseHolder.returnBadRequestAndMessage("Rezervace s tímto ID neexistuje!");
        } else {
            try {
                reservation.setState(State.valueOf(reservationState.getReservationState()));
                reservationRepository.save(reservation);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return ResponseHolder.returnBadRequestAndMessage("Neplatný stav rezervace!");
            } catch (Exception e) {
                return exceptionHandling.handleDatabaseException(e);
            }
            return ResponseHolder.returnEmptyBody(HttpServletResponse.SC_OK);
        }
    }

    public ResponseHolder cancelReservationByCode(String json) throws JsonProcessingException {
        String code = JsonToObjectParser.getStringValueFromNode(json, "code");

        List<Reservation> reservations = reservationRepository.findAll();

        for (Reservation reservation : reservations) {
            if (code.equals(reservation.getCodeForDelete())) {
                reservation.setState(State.DENIED);
                try {
                    reservationRepository.save(reservation);
                } catch (Exception e) {
                    return exceptionHandling.handleDatabaseException(e);
                }
                return ResponseHolder.returnOkAndMessage("Rezervace byla úspěšně zrušena.");
            }
        }

        return ResponseHolder.returnBadRequestAndMessage("Rezervaci na základě Vámi zadaného kódu nebylo možné bohužel zrušit, kontaktujte nás prosím telefonicky.");
    }
}
