package cz.vutbr.fit.PISBE.features.category;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.ExceptionHandling;
import cz.vutbr.fit.PISBE.common.JsonToObjectParser;
import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.features.category.jsonObjects.GetMenuDetailsJackson;
import cz.vutbr.fit.PISBE.features.category.jsonObjects.NewCategoryJackson;
import cz.vutbr.fit.PISBE.features.category.jsonObjects.ShowMenuJackson;
import cz.vutbr.fit.PISBE.features.category.jsonObjects.ShowOrderItemsJackson;
import cz.vutbr.fit.PISBE.features.menuItem.MenuItem;
import cz.vutbr.fit.PISBE.features.menuItem.MenuItemRepository;
import cz.vutbr.fit.PISBE.features.menuItem.jsonObjects.MenuItemJackson;
import cz.vutbr.fit.PISBE.features.orderItem.OrderItem;
import cz.vutbr.fit.PISBE.features.orderItem.OrderItemRepository;
import cz.vutbr.fit.PISBE.features.orderItem.State;
import cz.vutbr.fit.PISBE.features.orderItem.jsonObjects.OrderItemJackson;
import cz.vutbr.fit.PISBE.features.placeOfReservation.PlaceOfReservationRepository;
import cz.vutbr.fit.PISBE.features.user.UserServiceDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceDaoImpl implements CategoryServiceDao {
    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    MenuItemRepository menuItemRepository;

    @Autowired
    UserServiceDaoImpl userServiceDao;

    @Autowired
    OrderItemRepository orderItemRepository;

    @Autowired
    PlaceOfReservationRepository placeOfReservationRepository;

    @Autowired
    ExceptionHandling exceptionHandling;

    public ArrayList<ShowOrderItemsJackson> getAllOrderItemsAtPlace(List<Category> categories, int placeId) throws Exception{
        ArrayList<ShowOrderItemsJackson> placeOrderItems = new ArrayList<>();

        // go through every Category, find all OrderItems, which correspond to the category and placeId and fill them into
        // correct object (ShowOrderItemsJackson)
        for (Category category: categories){
            List<OrderItem> orderItems = orderItemRepository.findAllByCategoryIdAndPlaceId(category.getCategoryId(), placeId);
            ArrayList<OrderItemJackson> orderItemJacksons = new ArrayList<>();
            for (OrderItem orderItem: orderItems){
                if(orderItem.getState() != State.PAID){
                    MenuItem menuItem = orderItem.getMenuItemReference();
                    orderItemJacksons.add(new OrderItemJackson(orderItem.getOrderItemId(), menuItem.getMenuItemId(),
                            menuItem.getName(), menuItem.getPrice(), orderItem.getSpecialRequirement(),
                            orderItem.getState().name(), menuItem.getGrammage()));
                }
            }
            placeOrderItems.add(new ShowOrderItemsJackson(category.getName(), orderItemJacksons));
        }

        return placeOrderItems;
    }

    public ArrayList<ShowMenuJackson> getAllCategoriesWithMenuItems(List<Category> categories) throws Exception{
        ArrayList<ShowMenuJackson> allCategoriesWithMenuItems = new ArrayList<>();

        for (Category category : categories) {
            // get all menuItems which are referenced to certain category and parse all of them to Jackson object
            List<MenuItem> menuItems = menuItemRepository.findAllByCategoryReference(category);
            ArrayList<MenuItemJackson> responseAllMenuItems = new ArrayList<>();
            for (MenuItem menuItem: menuItems){
                if(menuItem.isInMenu()){
                    MenuItemJackson menuItemJackson = new MenuItemJackson(menuItem.getMenuItemId(), menuItem.getName(),
                            menuItem.getPrice(), menuItem.getGrammage(), menuItem.getDescription());
                    responseAllMenuItems.add(menuItemJackson);
                }
            }
            ShowMenuJackson responseCategory = new ShowMenuJackson(category.getName(), responseAllMenuItems);
            allCategoriesWithMenuItems.add(responseCategory);
        }

        return  allCategoriesWithMenuItems;
    }

    public ResponseHolder<ArrayList<ShowMenuJackson>> getMenu() {
        List<Category> categories = categoryRepository.findAll();
        ArrayList<ShowMenuJackson> responseAllCategories;
        try {
            responseAllCategories = getAllCategoriesWithMenuItems(categories);
        }
        catch (Exception e){
            return exceptionHandling.handleDatabaseException(e);
        }


        ResponseHolder<ArrayList<ShowMenuJackson>> responseHolder = new ResponseHolder<>();
        responseHolder.setResponseBody(responseAllCategories);
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        return responseHolder;
    }

    private GetMenuDetailsJackson getSortedMenuForChefEditing() throws Exception{
        List<Category> categories = categoryRepository.findAll();

        // create 2 Arraylists for categories in and not in menu
        ArrayList<ShowMenuJackson> categoriesInMenu = new ArrayList<>();
        ArrayList<ShowMenuJackson> categoriesNotInMenu = new ArrayList<>();

        for (Category category: categories){
            List<MenuItem> menuItems = menuItemRepository.findAllByCategoryReference(category);

            // create showMenu and Arraylists for in menu and not in menu categories
            ShowMenuJackson showMenuJacksonIn = new ShowMenuJackson();
            ShowMenuJackson showMenuJacksonNotIn = new ShowMenuJackson();
            ArrayList<MenuItemJackson> menuItemsResponseIn = new ArrayList<>();
            ArrayList<MenuItemJackson> menuItemsResponseNotIn = new ArrayList<>();

            // go through every menu item assigned to category and based on condition if in menu assign it to correct
            // Arraylist
            for (MenuItem menuItem: menuItems){
                if(menuItem.isInMenu()){
                    menuItemsResponseIn.add(new MenuItemJackson(menuItem.getMenuItemId(), menuItem.getName(), menuItem.getPrice()));
                }
                else{
                    menuItemsResponseNotIn.add(new MenuItemJackson(menuItem.getMenuItemId(), menuItem.getName(), menuItem.getPrice()));
                }
            }

            // add final Arraylists to reponse Arraylists
            if (!menuItemsResponseIn.isEmpty()){
                showMenuJacksonIn.setCategoryName(category.getName());
                showMenuJacksonIn.setMenuItems(menuItemsResponseIn);
                categoriesInMenu.add(showMenuJacksonIn);
            }
            if (!menuItemsResponseNotIn.isEmpty()){
                showMenuJacksonNotIn.setCategoryName(category.getName());
                showMenuJacksonNotIn.setMenuItems(menuItemsResponseNotIn);
                categoriesNotInMenu.add(showMenuJacksonNotIn);
            }
        }

        return new GetMenuDetailsJackson(categoriesInMenu, categoriesNotInMenu);
    }

    public ResponseHolder<GetMenuDetailsJackson> getMenuDetails(){
        if (!userServiceDao.canAccess("Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        ResponseHolder<GetMenuDetailsJackson> responseHolder = new ResponseHolder<GetMenuDetailsJackson>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        try {
            responseHolder.setResponseBody(getSortedMenuForChefEditing());
        }
        catch (Exception e){
            return exceptionHandling.handleDatabaseException(e);
        }

        return responseHolder;
    }

    public ResponseHolder createNewCategory(String requestBody){
        if (!userServiceDao.canAccess("Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        NewCategoryJackson requestBodyJackson;
        try {
            requestBodyJackson = JsonToObjectParser.parseJsonToObject(requestBody, NewCategoryJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje v těle requestu byly špatně vyplněny!");
        }

        Category newCategory = new Category(requestBodyJackson.getName());
        try {
            categoryRepository.save(newCategory);
        }
        catch (Exception e){
            return exceptionHandling.handleDatabaseException(e);
        }

        return ResponseHolder.returnEmptyBody(HttpServletResponse.SC_CREATED);
    }

    public ResponseHolder deleteCategory(String categoryName){
        if (!userServiceDao.canAccess("Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        Category category = categoryRepository.findByName(categoryName);
        if (category == null) {
            return ResponseHolder.returnBadRequestAndMessage("Položka category s tímto názvem neexistuje!");
        } else {
            categoryRepository.delete(category);
            return ResponseHolder.returnEmptyBody(HttpServletResponse.SC_OK);
        }
    }

    public ResponseHolder getAllCategories(){
        if (!userServiceDao.canAccess("Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        List<Category> allCategories = null;
        try {
            allCategories = categoryRepository.findAll();
        }
        catch (Exception e){
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Něco se pokazilo při přístupu k databázi!");
        }

        ResponseHolder responseHolder = new ResponseHolder();
        responseHolder.setResponseBody(allCategories);
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        return responseHolder;
    }
}
