package cz.vutbr.fit.PISBE.features.menuItem;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.vutbr.fit.PISBE.common.ExceptionHandling;
import cz.vutbr.fit.PISBE.common.JsonToObjectParser;
import cz.vutbr.fit.PISBE.common.ResponseHolder;
import cz.vutbr.fit.PISBE.features.category.Category;
import cz.vutbr.fit.PISBE.features.category.CategoryRepository;
import cz.vutbr.fit.PISBE.features.menuItem.jsonObjects.*;
import cz.vutbr.fit.PISBE.features.orderItem.OrderItem;
import cz.vutbr.fit.PISBE.features.orderItem.OrderItemRepository;
import cz.vutbr.fit.PISBE.features.user.UserServiceDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class MenuItemServiceDaoImpl implements MenuItemServiceDao {
    @Autowired
    MenuItemRepository menuItemRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    UserServiceDaoImpl userServiceDao;

    @Autowired
    ExceptionHandling exceptionHandling;

    @Autowired
    OrderItemRepository orderItemRepository;

    private ArrayList<MenuItemWithCategoryJackson> getAllMenuItemsByCategory() {
        ArrayList<MenuItemWithCategoryJackson> menuItemWithCategoryJackson = new ArrayList<>();
        List<Category> categories = categoryRepository.findAll();

        for (Category category : categories) {

            List<MenuItem> allMenuItemsByCategory = menuItemRepository.findAllByCategoryReference(category);

            ArrayList<MenuItemJackson> menuItemJacksonsArray = new ArrayList<>();

            for (MenuItem menuItem : allMenuItemsByCategory) {
                MenuItemJackson menuItemJackson = new MenuItemJackson();
                menuItemJackson.setMenuItemId(menuItem.getMenuItemId());
                menuItemJackson.setName(menuItem.getName());
                menuItemJackson.setPrice(menuItem.getPrice());
                menuItemJackson.setGrammage(menuItem.getGrammage());
                menuItemJackson.setDescription(menuItem.getDescription());

                menuItemJacksonsArray.add(menuItemJackson);

            }
            menuItemWithCategoryJackson.add(new MenuItemWithCategoryJackson(category.getName(), menuItemJacksonsArray));
        }
        return menuItemWithCategoryJackson;
    }

    public ResponseHolder getAllMenuItems() {
        if ((!userServiceDao.canAccess("Cook,Chef"))) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        ArrayList<MenuItemWithCategoryJackson> menuItemWithCategoryJackson = getAllMenuItemsByCategory();

        ResponseHolder<ArrayList<MenuItemWithCategoryJackson>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(menuItemWithCategoryJackson);
        return responseHolder;
    }

    public ResponseHolder getAllCategoriesForMenuItemForm() {
        if (!userServiceDao.canAccess("Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        List<Category> categories = categoryRepository.findAll();
        ArrayList<CategoriesJackson> allCategories = new ArrayList<>();

        for (Category category : categories) {
            allCategories.add(new CategoriesJackson(category.getCategoryId(), category.getName()));
        }

        ResponseHolder<ArrayList<CategoriesJackson>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(allCategories);
        return responseHolder;
    }

    public ResponseHolder createNewMenuItem(String requestBody) {
        if (!userServiceDao.canAccess("Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        MenuItemNewJackson requestBodyJackson;
        try {
            requestBodyJackson = JsonToObjectParser.parseJsonToObject(requestBody, MenuItemNewJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje v těle requestu byly špatně vyplněny!");
        }

        String grammage = requestBodyJackson.getGrammage();
        if (!grammage.matches("^(\\d{1,4}|\\d[.,]\\d{1,2}) (ks|ml|g|kg|l)$")){
            return ResponseHolder.returnBadRequestAndMessage("Hmotnost má špatný formát, povolena jsou 1-4 čísla následovaná mezerou a jednotkou!");
        }

        MenuItem menuItem = new MenuItem(requestBodyJackson.getPrice(), requestBodyJackson.getProductionPrice(), requestBodyJackson.getName(),
                requestBodyJackson.getGrammage(), requestBodyJackson.getDescription(), false);

        Category category = categoryRepository.findByCategoryId(requestBodyJackson.getCategoryId());

        if (category == null) {
            return ResponseHolder.returnBadRequestAndMessage("Chybně zadaná kategorie pokrmu");
        }
        menuItem.setCategoryReference(category);

        List<MenuItem> menuItems = menuItemRepository.findAll();

        for (MenuItem tempMenuItem : menuItems) {
            if (menuItem.getName().equals(tempMenuItem.getName())) {
                return ResponseHolder.returnBadRequestAndMessage("Položka s tímto názvem již existuje.");
            }
        }

        try {
            menuItemRepository.save(menuItem);
        } catch (Exception e) {
            return exceptionHandling.handleDatabaseException(e);
        }
        return ResponseHolder.returnOkAndMessage("Prvek menu byl úspěšně vytvořen.");
    }


    public ResponseHolder getMenuItem(int menuItemId) {
        if ((!userServiceDao.canAccess("Cook")) && (!userServiceDao.canAccess("Chef")) && (!userServiceDao.canAccess("Waiter"))) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        MenuItem menuItem = menuItemRepository.findByMenuItemId(menuItemId);
        if (menuItem == null) {
            return ResponseHolder.returnBadRequestAndMessage("Pokrm s tímto ID neexistuje!");
        }

        FullMenuItemJackson fullMenuItemJackson = new FullMenuItemJackson(menuItem.getMenuItemId(), menuItem.getName(),
                menuItem.getPrice(), menuItem.getProductionPrice(), menuItem.getGrammage(), menuItem.getDescription(), menuItem.getCategoryReference().getName(),
                false);

        ResponseHolder<FullMenuItemJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(fullMenuItemJackson);
        return responseHolder;
    }


    public ResponseHolder editMenuItem(String requestBody, int menuItemId) {
        if (!userServiceDao.canAccess("Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        MenuItemNewJackson menuItemNewJackson;
        try {
            menuItemNewJackson = JsonToObjectParser.parseJsonToObject(requestBody, MenuItemNewJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje v těle requestu byly špatně vyplněny!");
        }

        MenuItem menuItem = menuItemRepository.findByMenuItemId(menuItemId);
        menuItem.setName(menuItemNewJackson.getName());
        menuItem.setPrice(menuItemNewJackson.getPrice());
        menuItem.setProductionPrice(menuItemNewJackson.getProductionPrice());
        menuItem.setGrammage(menuItemNewJackson.getGrammage());
        menuItem.setDescription(menuItemNewJackson.getDescription());
        menuItem.setCategoryReference(categoryRepository.findByCategoryId(menuItemNewJackson.getCategoryId()));

        try {
            menuItemRepository.save(menuItem);
        } catch (Exception e) {
            return exceptionHandling.handleDatabaseException(e);
        }
        return ResponseHolder.returnOkAndMessage("Editace proběhla korektně.");
    }


    public ResponseHolder deleteMenuItem(int menuItemId) {
        if (!userServiceDao.canAccess("Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        MenuItem menuItem = menuItemRepository.findByMenuItemId(menuItemId);
        if (menuItem == null) {
            return ResponseHolder.returnBadRequestAndMessage("Položka menu s tímto ID neexistuje!");
        } else {
            menuItemRepository.delete(menuItem);
            return ResponseHolder.returnEmptyBody(HttpServletResponse.SC_OK);
        }
    }

    public ResponseHolder editMenuPost(String requestBody) {
        if (!userServiceDao.canAccess("Chef")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        MenuDetailJackson requestBodyJackson;
        try {
            requestBodyJackson = JsonToObjectParser.parseJsonToObject(requestBody, MenuDetailJackson.class);
        } catch (JsonProcessingException e) {
            return ResponseHolder.returnBadRequestAndMessage("Některé údaje v těle requestu byly špatně vyplněny!");
        }

        ArrayList<MenuItemWithCategoryJackson> categoriesInMenu = requestBodyJackson.getCategoriesInMenu();
        ArrayList<MenuItemWithCategoryJackson> categories = requestBodyJackson.getCategories();

        for (MenuItemWithCategoryJackson menuItemWithCategoryJackson : categories) {
            ArrayList<MenuItemJackson> menuItemJacksonsArray = (ArrayList<MenuItemJackson>) menuItemWithCategoryJackson.getMenuItems();
            for (MenuItemJackson menuItemJackson : menuItemJacksonsArray) {
                MenuItem menuItem = menuItemRepository.findByMenuItemId(menuItemJackson.getMenuItemId());
                menuItem.setInMenu(false);
                try {
                    menuItemRepository.save(menuItem);
                } catch (Exception e) {
                    return exceptionHandling.handleDatabaseException(e);
                }
            }
        }

        for (MenuItemWithCategoryJackson menuItemWithCategoryJackson : categoriesInMenu) {
            ArrayList<MenuItemJackson> menuItemJacksonsArray = (ArrayList<MenuItemJackson>) menuItemWithCategoryJackson.getMenuItems();
            for (MenuItemJackson menuItemJackson : menuItemJacksonsArray) {
                MenuItem menuItem = menuItemRepository.findByMenuItemId(menuItemJackson.getMenuItemId());
                menuItem.setInMenu(true);
                try {
                    menuItemRepository.save(menuItem);
                } catch (Exception e) {
                    return exceptionHandling.handleDatabaseException(e);
                }
            }
        }

        return ResponseHolder.returnEmptyBody(HttpServletResponse.SC_OK);
    }

    public ResponseHolder getSalesOfMenuItems() {
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        List<Object> menuItemSales;
        ArrayList<MenuItemSalesJackson> itemSalesResponse = new ArrayList<>();
        try {
            menuItemSales = menuItemRepository.getCountOfSoldMenuItems();
            for (Object menuItemSale : menuItemSales) {
                Object[] attribs = (Object[]) menuItemSale;
                String description = (String) attribs[3];
                if (description == null) {
                    description = "";
                }
                itemSalesResponse.add(new MenuItemSalesJackson(
                        ((BigDecimal) attribs[6]).intValue(), (String) attribs[5],
                        ((BigDecimal) attribs[0]).doubleValue(), ((BigDecimal) attribs[1]).doubleValue(),
                        ((BigDecimal) attribs[2]).intValue(), (String) attribs[4], description, ((BigDecimal) attribs[7]).intValue()));
            }
        } catch (Exception e) {
            return exceptionHandling.handleDatabaseException(e);
        }

        ResponseHolder<ArrayList<MenuItemSalesJackson>> responseHolder = new ResponseHolder<ArrayList<MenuItemSalesJackson>>();
        responseHolder.setResponseBody(itemSalesResponse);
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        return responseHolder;
    }

    public HashMap<String, MenuItemSalesJackson> getSaleTimesOfAllMenuItems() {
        List<Object> menuItemSalesTimes = menuItemRepository.getTimesOfSale();
        // tmp map - {'menu_item_name': [MenuItemSalesJackson]}
        HashMap<String, MenuItemSalesJackson> respMap = new HashMap<>();

        for (Object menuItemSale : menuItemSalesTimes) {
            Object[] attribs = (Object[]) menuItemSale;
            try {
                // 5 index is name of MenuItem, 7 is created At of OrderItem
                respMap.get((String) attribs[5]).updateSoldTimes(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(attribs[7]));
            } catch (NullPointerException e) {
                String description = (String) attribs[3];
                if (description == null) {
                    description = "";
                }
                ArrayList<String> salesTimes = new ArrayList<>();
                salesTimes.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(attribs[7]));
                MenuItemSalesJackson salesJackson = new MenuItemSalesJackson(
                        ((BigDecimal) attribs[6]).intValue(), (String) attribs[5],
                        ((BigDecimal) attribs[0]).doubleValue(), ((BigDecimal) attribs[1]).doubleValue(),
                        ((BigDecimal) attribs[2]).intValue(), (String) attribs[4], description, salesTimes);
                respMap.put(salesJackson.getName(), salesJackson);
            }
        }
        return respMap;
    }

    public ResponseHolder getTimesOfSale() {
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        ArrayList<MenuItemSalesJackson> itemSalesResponse = new ArrayList<>();

        // tmp map - {'menu_item_name': [MenuItemSalesJackson]}
        HashMap<String, MenuItemSalesJackson> respMap;
        try {
            respMap = getSaleTimesOfAllMenuItems();
        } catch (Exception e) {
            return exceptionHandling.handleDatabaseException(e);
        }

        for (Map.Entry<String, MenuItemSalesJackson> entry : respMap.entrySet()) {
            itemSalesResponse.add(entry.getValue());
        }

        ResponseHolder<ArrayList<MenuItemSalesJackson>> responseHolder = new ResponseHolder<>();
        responseHolder.setResponseBody(itemSalesResponse);
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        return responseHolder;
    }

    public ResponseHolder getSalesForPeriod(int year, int month) {
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }


        List<Object> menuItemSales = menuItemRepository.getAllOrders();
        Double selectedSales = 0.0;

        for (Object menuItemSale : menuItemSales) {
            Object[] items = (Object[]) menuItemSale;
            Calendar cal = Calendar.getInstance();
            Date date = (Date) items[0];
            cal.setTime(date);
            int monthForSale = cal.get(Calendar.MONTH) + 1;
            int yearForSale = cal.get(Calendar.YEAR);
            ;

            if ((monthForSale == month) && (yearForSale == year)) {
                selectedSales += (Double.parseDouble(String.valueOf(items[1])) - Double.parseDouble(String.valueOf(items[2])));
            }
        }

        ResponseHolder<Double> responseHolder = new ResponseHolder<>();
        responseHolder.setResponseBody(selectedSales);
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        return responseHolder;
    }

    public ResponseHolder getLastTimeSold(){
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        ArrayList<MenuItemSalesJackson> itemSalesResponse = new ArrayList<>();

        // tmp map - {'menu_item_name': [MenuItemSalesJackson]}
        HashMap<String, MenuItemSalesJackson> respMap;
        try {
            respMap = getSaleTimesOfAllMenuItems();
        }
        catch (Exception e){
            return exceptionHandling.handleDatabaseException(e);
        }

        for (Map.Entry<String, MenuItemSalesJackson> entry: respMap.entrySet()){
            MenuItemSalesJackson menuItemSalesJackson = entry.getValue();
            menuItemSalesJackson.sortSoldTimes();
            menuItemSalesJackson.setLastTimeSold(menuItemSalesJackson.getSoldTimes().get(menuItemSalesJackson.getSoldTimes().size()-1));
            menuItemSalesJackson.setSoldTimes(null);
            itemSalesResponse.add(entry.getValue());
        }

        ResponseHolder responseHolder = new ResponseHolder();
        responseHolder.setResponseBody(itemSalesResponse);
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        return responseHolder;
    }

    public ResponseHolder getUtilization() {
        if (!userServiceDao.canAccess("Manager")) {
            return ResponseHolder.returnReturnCodeAndMessage(HttpServletResponse.SC_FORBIDDEN, "Uživatel nemá " +
                    "dostatečná oprávnění pro přístup!");
        }

        List<Integer> orderItemSales = menuItemRepository.getAllOrdersOnlyId();
        ArrayList<OrderItem> orderItems = new ArrayList<>();

        for (int orderItemSale : orderItemSales) {
            orderItems.add(orderItemRepository.findByOrderItemId(orderItemSale));
        }

        orderItems.sort(Comparator.comparing(OrderItem::getCreatedAt));
        ArrayList<Integer> allHours = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            int averageOfOrderItemsForConcreteHour = 0;
            for (OrderItem orderItem : orderItems) {
                Date date = orderItem.getCreatedAt();
                int hourForOrderItem = date.getHours();
                if (hourForOrderItem == i) {
                    averageOfOrderItemsForConcreteHour++;
                }
            }
            allHours.add(averageOfOrderItemsForConcreteHour);
        }

        int sumOfAllHours = 0;
        for (int hour : allHours) {
            sumOfAllHours += hour;
        }

        ArrayList<Long> ratioOfHours = new ArrayList<>();

        for (int hour : allHours) {
            ratioOfHours.add(Math.round(((double)hour/(double)sumOfAllHours)*100));
        }

        ResponseHolder<ArrayList<Long>> responseHolder = new ResponseHolder<>();
        responseHolder.setResponseBody(ratioOfHours);
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        return responseHolder;
    }
}
