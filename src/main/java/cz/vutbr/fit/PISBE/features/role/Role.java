package cz.vutbr.fit.PISBE.features.role;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "roleID")
    private int roleId;

    @NotNull(message = "Typ role nebyl zadán!")
    @Column(name = "type")
    private String type;

    public Role(){

    }

    public Role(@NotNull String type) {
        this.type = type;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

