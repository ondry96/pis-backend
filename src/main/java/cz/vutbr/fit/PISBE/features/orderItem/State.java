package cz.vutbr.fit.PISBE.features.orderItem;

public enum State {
    WAITING, PREPARING, FINISHED, PAID, RELEASED
}
