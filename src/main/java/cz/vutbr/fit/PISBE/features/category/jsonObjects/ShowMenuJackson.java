package cz.vutbr.fit.PISBE.features.category.jsonObjects;

import cz.vutbr.fit.PISBE.features.menuItem.jsonObjects.MenuItemJackson;

import java.util.ArrayList;

public class ShowMenuJackson {
    private String categoryName;
    private ArrayList<MenuItemJackson> menuItems;

    public ShowMenuJackson() {
    }

    public ShowMenuJackson(String categoryName, ArrayList<MenuItemJackson> menuItems) {
        this.categoryName = categoryName;
        this.menuItems = menuItems;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<MenuItemJackson> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(ArrayList<MenuItemJackson> menuItems) {
        this.menuItems = menuItems;
    }
}
