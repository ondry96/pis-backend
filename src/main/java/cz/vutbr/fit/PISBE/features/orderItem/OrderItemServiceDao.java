package cz.vutbr.fit.PISBE.features.orderItem;

import cz.vutbr.fit.PISBE.common.ResponseHolder;

public interface OrderItemServiceDao {
    ResponseHolder getAllOrderItems();
    ResponseHolder createNewOrderItem(int placeId, int menuItemId, String requestBody);
    ResponseHolder addSpecialRequirement(int placeId, int orderItemId, String requestBody);
    ResponseHolder deleteOrderItem(int placeId, int orderItemId);
    ResponseHolder markOrderItemAsPreparing(int orderItemId);
    ResponseHolder markOrderItemAsFinished(int orderItemId);
    ResponseHolder markOrderItemAsReleased(int orderItemId);
    ResponseHolder getOrderItemForm(int placeId);
}
