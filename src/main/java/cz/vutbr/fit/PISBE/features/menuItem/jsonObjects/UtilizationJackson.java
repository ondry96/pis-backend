package cz.vutbr.fit.PISBE.features.menuItem.jsonObjects;

public class UtilizationJackson {
    private int hour;
    private int averageNumberOfOrders;

    public UtilizationJackson() {
    }

    public UtilizationJackson(int hour, int averageNumberOfOrders) {
        this.hour = hour;
        this.averageNumberOfOrders = averageNumberOfOrders;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getAverageNumberOfOrders() {
        return averageNumberOfOrders;
    }

    public void setAverageNumberOfOrders(int averageNumberOfOrders) {
        this.averageNumberOfOrders = averageNumberOfOrders;
    }
}
