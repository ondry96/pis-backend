package cz.vutbr.fit.PISBE.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonToObjectParser {

    public static <T> T parseJsonToObject(String json, Class<T> mappedObject) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, mappedObject);
    }

    public static String getStringValueFromNode(String json, String nodeName) throws JsonProcessingException {
        ObjectNode objectNode;
        objectNode = JsonToObjectParser.parseJsonToObject(json, ObjectNode.class);
        JsonNode node = objectNode.get(nodeName);
        return node.textValue();
    }
}
